const ProgressFieldComponent = ({ source, record = {}, data }) => {

  const sourceData = get(record,source);

  let component = [];
  data.forEach((d,i) => component.push(<div key={d} style={{
    width: '15px',
    height: '15px',
    background:  sourceData.includes(d) ? 'green' : 'grey',
    marginRight: '5px',
    borderRadius: '50%'
  }}
  />))
 
  return (<div style={{ display: 'flex' }}>
       {component}
    </div>
    )
}


PureTextField.defaultProps = {
  addLabel: true,
  data:[]
};
