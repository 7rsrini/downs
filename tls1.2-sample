  @Bean(name = "restTemplate")
    @Profile({"default","development"})
    public RestTemplate restTemplateDefault() throws InternalServerException {
        return new RestTemplate(clientHttpRequestFactoryDefault());
    }

    @Bean(name = "restTemplate")
    @Profile({"acceptance","production"})
    public RestTemplate restTemplate() throws InternalServerException {
        return new RestTemplate(clientHttpRequestFactory());
    }



    private ClientHttpRequestFactory clientHttpRequestFactoryDefault() throws InternalServerException{
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setReadTimeout(configurations.getTandemReadTimeout());
        factory.setConnectTimeout(configurations.getTandemConnectTimeout());
        return factory;
    }


    private ClientHttpRequestFactory clientHttpRequestFactory() throws InternalServerException{
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient());
        factory.setReadTimeout(configurations.getTandemReadTimeout());
        factory.setConnectTimeout(configurations.getTandemConnectTimeout());
        return factory;
    }


    private HttpClient httpClient() throws InternalServerException{

        String keystorePassword = configurations.getKeyStorePassword();
        Resource keyStoreResource = resourceLoader.getResource("classpath:"+configurations.getCertificateFile());

        try{
            KeyStore trustStore = KeyStore.getInstance("JCEKS");
            // Note: Java 8 use TLS1.2 by default.
            SSLContextBuilder sslcontextBuilder = SSLContexts.custom().useProtocol("TLS")
                    .loadTrustMaterial(trustStore, (X509Certificate[] chain, String authType)-> true);
            FileInputStream fis = new FileInputStream(keyStoreResource.getFile());
            KeyStore ks = KeyStore.getInstance("JCEKS");
            ks.load(fis, keystorePassword.toCharArray());
            sslcontextBuilder.loadKeyMaterial(ks, keystorePassword.toCharArray());

            SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslcontextBuilder.build(),(String hostname, SSLSession session) -> true);
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create().register("https", sslConnectionSocketFactory).build();
            PoolingHttpClientConnectionManager poolingManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            poolingManager.setMaxTotal(100);
            poolingManager.setDefaultMaxPerRoute(25);
            HttpClientBuilder builder = HttpClients.custom();
            builder.setConnectionManager(poolingManager);
            return builder.build();

        }catch (GeneralSecurityException | IOException e){
            throw new InternalServerException("Failed to create http client.",e.getMessage(),e);
        }

    }